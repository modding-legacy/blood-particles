package com.legacy.blood_particles;

import org.apache.commons.lang3.tuple.Pair;

import net.minecraftforge.fml.ExtensionPoint;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.network.FMLNetworkConstants;

@Mod(BloodParticlesMod.MODID)
public class BloodParticlesMod
{
	public static final String MODID = "blood_particles";

	public BloodParticlesMod()
	{
		ModLoadingContext.get().registerExtensionPoint(ExtensionPoint.DISPLAYTEST, () -> Pair.of(() -> FMLNetworkConstants.IGNORESERVERONLY, (s, b) -> true));

		/*ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, BloodParticlesConfig.spec);*/
	}
}
